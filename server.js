const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
//carga los valores con la configuración por defecto
require('dotenv').config();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

//pasa a json todo lo que llega en el body
app.use(express.json());
app.use(enableCORS);

//Incluyo los módulos del UserController
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

app.listen(port);

console.log("API escuchando en el puerto BIP BP" + port);

app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }
)

/* Ejemplo de get
app.get('/apitechu/v1/users',
   function(req, res) {
     console.log("GET /apitechu/v1/users");

// Al poner root: __dirname se coloca en el directorio en el que está server.js
      res.sendFile('usuarios.json', {root: __dirname});

//otra forma de hacer el envío. Elegir una de las dos send o sendFile
      var users = require('./usuarios.json');
      res.send(users);

   }
)
Ejemlo de get */

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.get('/apitechu/v2/account/:idUsuario', accountController.getAccountByIdV2);


app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);

//Prueba detele tipo 1: Bucle for tipo 1
app.delete("/apitechu/v1/users/for1/:id", userController.deleteUserV1);

app.post('/apitechu/v1/login', authController.loginUserV1);
app.post('/apitechu/v2/login', authController.loginUserV2);

app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);
app.post('/apitechu/v2/logout/:id', authController.logoutUserV2);

/*
// Ejemplo de clase /
app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("Id es " + req.params.id);

//recupero el fichero de usuarios
    var users = require('./usuarios.json');
//quita el elemento id de la lista. Splice cambia el array y lo salva
    users.splice(req.params.id - 1, 1);
    console.log("Usuario quitado de la lista");
    writeUserDatatoFile(users);

    res.send({"msg" : "Usuario borrado con éxito"})
  }
)
*/


/*

//Prueba detele tipo 2: Bucle forEach
app.delete("/apitechu/v1/users/for2/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/for2/:id");
    console.log("Id es " + req.params.id);
    var users = require('./usuarios.json');

//Bucle for tipo 2
    var i = 0;
    var deleted = false;
    var user;
    users.forEach(function(user,i){
      console.log("i: " + i);
      console.log("ID del usuario i: " + user.id);
      if (user.id == req.params.id){
        console.log("El usuario que voy a borrar es: " + user.id);
        users.splice(i, 1);
        deleted = true;
      }
    });

    if (deleted){
      console.log("Usuario quitado de la lista");
      writeUserDatatoFile(users);
      res.send({"msg" : "Usuario borrado con éxito"});
    } else {
      res.send({"msg" : "Usuario no encontrado"})
    }
  }
)

//Prueba detele tipo 3: Bucle for in
app.delete("/apitechu/v1/users/for3/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/for3/:id");
    console.log("Id es " + req.params.id);
    var users = require('./usuarios.json');

//Bucle for tipo 3
    var deleted = false;
    for(arrayId in users){
      console.log("arrayId: " + arrayId);
      console.log("ID del usuario arrayId: " + users[arrayId].id);
      if (users[arrayId].id == req.params.id){
        console.log("El usuario que voy a borrar es: " + users[arrayId].id);
        users.splice(arrayId, 1);
        deleted = true;
      }
    };

    if (deleted){
      console.log("Usuario quitado de la lista");
      writeUserDatatoFile(users);
      res.send({"msg" : "Usuario borrado con éxito"});
    } else {
      res.send({"msg" : "Usuario no encontrado"})
    }
  }
)

//Prueba detele tipo 4: Bucle for of usando user.entries()
app.delete("/apitechu/v1/users/for4entries/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/for4entries/:id");
    console.log("Id es " + req.params.id);
    var users = require('./usuarios.json');

//Bucle for tipo 4. users.entries() devuelve la pareja indice valor de cada elemento
    var deleted = false;
    for(var[index, user] of users.entries()){
      console.log("index: " + index);
      console.log("ID del usuario user.id: " + user.id);
      if (user.id == req.params.id){
        console.log("El usuario que voy a borrar es: " + user.id);
        users.splice(index, 1);
        deleted = true;
      }
    };

    if (deleted){
      console.log("Usuario quitado de la lista");
      writeUserDatatoFile(users);
      res.send({"msg" : "Usuario borrado con éxito"});
    } else {
      res.send({"msg" : "Usuario no encontrado"})
    }
  }
)

//Prueba delete tipo 4: Bucle for of usando index++
app.delete("/apitechu/v1/users/for4acumula/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/for4acumula/:id");
    console.log("Id es " + req.params.id);
    var users = require('./usuarios.json');

//Bucle for tipo 4. users.entries() lleva un contador para seguir el índice
    var index = 0;
    var deleted = false;
    for(user of users){
      console.log("index: " + index);
      console.log("ID del usuario user.id: " + user.id);
      if (user.id == req.params.id){
        console.log("El usuario que voy a borrar es: " + user.id);
        users.splice(index, 1);
        deleted = true;
      }
      index++;
    };

    if (deleted){
      console.log("Usuario quitado de la lista");
      writeUserDatatoFile(users);
      res.send({"msg" : "Usuario borrado con éxito"});
    } else {
      res.send({"msg" : "Usuario no encontrado"})
    }
  }
)

//Prueba delete tipo 5: Bucle for usando findIndex
app.delete("/apitechu/v1/users/for5/:id",
  function(req, res){
    console.log("DELETE /apitechu/v1/users/for5/:id");
    console.log("Id es " + req.params.id);
    var users = require('./usuarios.json');

//Bucle for tipo 5
    var deleted = false;
    var indexOfElement = users.findIndex(
      function(element){
        return element.id == req.params.id
      }
    )

    if (indexOfElement > 0) {
      console.log("Indice del elemento borrado: " + indexOfElement);
      users.splice(indexOfElement, 1);
      deleted = true;
    }

    if (deleted){
      console.log("Usuario quitado de la lista");
      writeUserDatatoFile(users);
      res.send({"msg" : "Usuario borrado con éxito"});
    } else {
      res.send({"msg" : "Usuario no encontrado"})
    }
  }
)

*/


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);


  }
)
