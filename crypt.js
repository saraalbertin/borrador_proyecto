const bcrypt = require('bcrypt');

//devuelve un string encriptado. Encripta data
function hash(data) {
  console.log("hashing data");
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
 console.log("Checking password");

 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
