const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusga10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
//pongo ../usuarios.json para subir en el directorio
  var users = require('../usuarios.json');
//    console.log(users);

  if (req.query.$count == "true"){
    console.log("Recuento solicitado");
    result.count = users.length;
  }

  if (req.query.$top) {
    result.users = users.slice(0, req.query.$top);
  } else {
    result.users = users;
  }
  res.send(result);
  console.log("Lista enviada");

}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

// crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body: {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }

  )

}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La consulta es" + query);

// crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function createUserV1(req, res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDatatoFile(users);
  console.log("Usuario añadido con éxito");

  res.send({"msg": "Usuario añadido con éxito"})

}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario creado en MLAb");
      res.status(201).send({"msg":"Usuario guardado"});
    }
  )
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/for1/:id");
  console.log("Id es " + req.params.id);
//recupero el fichero de usuarios
  var users = require('../usuarios.json');
//Bucle for
  var i;
  deleted = false;
  for (i = 0; i < users.length; i++) {
    console.log("Comparo users[i].id " + users[i].id + " y req.params.id " + req.params.id);
    if (users[i].id == req.params.id){
      console.log("El usuario que voy a borrar es: " + users[i].id);
      users.splice(i, 1);
      deleted = true;
      break;
    }
  };
  console.log("i a la salida " + i);
  if (deleted){
    io.writeUserDatatoFile(users);
    res.send({"msg" : "Usuario borrado con éxito"});
  } else {
    res.send({"msg" : "Usuario no encontrado"})
  }
}


// se pone para poder utilizar la función desde fuera
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
