const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusga10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function loginUserV1(req, res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var result = {};
  var i;
  encontrado = false;
  for (i = 0; i < users.length; i++) {
    //console.log("Comparo users[i].email " + users[i].email + " y req.body.email " + req.body.email);
    if (users[i].email == req.body.email && users[i].password == req.body.password){
      console.log("El usuario encontrado: " + users[i].email);
      console.log("El usuario password: " + users[i].password);
      encontrado = true;
      users[i].logged = true;
      result.id = users[i].id;
      break;
    }
  };

  if (encontrado){
    io.writeUserDatatoFile(users);
    //Login correcto { "mensaje" : "Login correcto", "idUsuario" : 1 }
    res.send({"mensaje" : "login correcto" , "idUsuario" : result.id});
  } else {
    res.send({"msg" : "Login incorrecto"})
  }
}

function loginUserV2(req, res) {
  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        var response = bodyGet[0];
        console.log("req.body.password: " + req.body.password);
        console.log("bodyGet.password: " + bodyGet[0].password);
        var encontrado = crypt.checkPassword(req.body.password, bodyGet[0].password);
        console.log("Encontrado: " + encontrado);
        if (encontrado) {
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey,
            JSON.parse(putBody))
          var response = {"msg" : "login correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {"msg" : "Password incorrecta"}
          res.status(400);
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
        res.status(500);
      }
      res.send(response);
    }
  )

}


function logoutUserV1(req, res) {
  console.log("POST /apitechu/v1/logout/:id");
  console.log(req.params.id);

  var users = require('../usuarios.json');
  var result = {};
  var i;
  encontrado = false;
  for (i = 0; i < users.length; i++) {
    //console.log("Comparo users[i].email " + users[i].email + " y req.body.email " + req.body.email);
    if (users[i].id == req.params.id && users[i].logged == true){
      console.log("El usuario encontrado: " + users[i].id);
      //console.log("El usuario password: " + users[i].password);
      encontrado = true;
      delete users[i].logged;
      result.id = users[i].id;
      break;
    }
  };

  if (encontrado){
    io.writeUserDatatoFile(users);
    res.send({"mensaje" : "logout correcto" , "idUsuario" : result.id});
  } else {
    res.send({"msg" : "Logout incorrecto"})
  }
}

function logoutUserV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");
  console.log(req.params.id);

  var query = 'q={"id": ' + req.params.id + '}';
  console.log("La consulta es " + query);

  // crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {
      if (bodyGet.length > 0) {
        console.log("Usuario encontrado: " + req.params.id);

        if (bodyGet[0].logged == true) {
          console.log("Usuario conectado");
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("user?" + query + "&" + mLabAPIKey,
             JSON.parse(putBody))
          var response = {"mensaje" : "logout correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {"msg" : "Usuario no conectado"}
        }
      } else {
        var response = {"msg" : "Usuario no encontrado"}
      }
      res.send(response);
    }
  )
}

/////////
/*

  if (encontrado){
    io.writeUserDatatoFile(users);
    res.send({"mensaje" : "logout correcto" , "idUsuario" : result.id});
  } else {
    res.send({"msg" : "Logout incorrecto"})
  }
}
*/
///////////

module.exports.loginUserV1 = loginUserV1;
module.exports.loginUserV2 = loginUserV2;
module.exports.logoutUserV1 = logoutUserV1;
module.exports.logoutUserV2 = logoutUserV2;
