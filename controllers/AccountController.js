const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechusga10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../crypt');

function getAccountByIdV2(req, res) {
  console.log("GET /apitechu/v2/account:idUsuario");

  var idUsuario = req.params.idUsuario;
  var query = 'q={"idUsuario":' + idUsuario + '}';
  console.log("La consulta es" + query);

// crea cliente http
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo cuenta"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Usuario sin cuentas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

// se pone para poder utilizar la función desde fuera
module.exports.getAccountByIdV2 = getAccountByIdV2;
